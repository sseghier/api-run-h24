# API RUN H24



## Sujet du projet
L'idée est de créer deux instances des applications gotosocial et pinafore pour obtenir un modèle client-serveur sur le VPS.

## Gotosocial ( Méthode Manuelle )
- Préparer le VPS : 


`mkdir /gotosocial && mkdir /gotosocial/storage && mkdir /gotosocial/storage/certs`

- Récupérer la dernière version : 


`wget https://github.com/superseriousbusiness/gotosocial/releases/download/v0.5.2/gotosocial_0.5.2_linux_amd64.tar.gz`

- Extraire : 


`tar -xzf gotosocial_0.5.2_linux_amd64.tar.gz`

- Editer le fichier config.yaml


host => hostname du vps

port => 8058

db-type => postgres

db-address => localhost

storage-local-base-path => /gotosocial/storage

letsencrypt-enabled => false

- Créer un service systemd


`sudo useradd -r gotosocial`


`sudo groupadd gotosocial`


`sudo usermod -a -G gotosocial gotosocial`


`sudo chown -R gotosocial:gotosocial /gotosocial`


`sudo cp /gotosocial/example/gotosocial.service /etc/systemd/system/`

`sudo systemctl enable --now gotosocial.service`


## Pinafore (Méthode Docker)
 
- Installer la dernière version : 


` sudo wget https://github.com/nolanlawson/pinafore/archive/refs/tags/v2.6.0.tar.gz`



- Installer les dépendences :


`sudo apt-get install nodejs`

`sudo apt-get install npm`

` curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -`

` echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list`


- Build le projet via Docker : 


`docker build .`


`docker run -d -p 4002:4002 [your-image]`


## Configuration Nginx

- Utiliser nginx comme Reverse-Proxy sur les ports utilisés par les instances. 

`proxy_pass http://127.0.0.1:4002;`

Cette commande sert à rediriger les requêtes sur le port 4002 vers ...

` server_name php.111.picagraine.net;`

... le nom de domaine php.111.picagraine.net.

Les noms de domaines ne sont pas représentatifs car il y a eu un souci avec Certbot pour obtenir des certificats sur le nom de domaine picagraine. On "recycle" donc les certificats précédents en conservant les noms de domaines précédents.

Sinon pour générer un certificat html on fait : 

`sudo certbot --nginx`

Puis on sélectionne le bon nom de domaine.

## Etat actuel 

Les deux instances fonctionnent bien, et d'un point de vue personnel j'ai pu apprendre comment utiliser docker dans le cadre de l'API. J'ai pu revoir les bases de Linux, de l'administration système et de l'hébergement tout en approfondissant au moyen de divers exercices pour arriver au projet qui rassemble toutes les connaissances acquises au cours de la semaine.